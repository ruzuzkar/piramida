<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_piramida');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%d,K1,hK#TP*A#gE<(w@V3-zazX&%]d:Lbs%ej^cJ{TlBe)gmn}z@vO.F}.UT2Lc');
define('SECURE_AUTH_KEY',  '*>$Cwv/1, TfEvWUtH6=t-7/,D=3;BcYyl&AV6YQqQ$qCQL^>`K4Kuh>r`MDGcdj');
define('LOGGED_IN_KEY',    'Onz=4~ZKg>h#+|;QEzn75CilZJ 5N`wD+6b=J;<x;269T!xpgh-<&&`*C(WpzKdZ');
define('NONCE_KEY',        ']^b3u,?+iV#oJs#R/VP8j#kUjj.>(NKwCBQ37KO_fqk[U&k2Vg1D5cu6*wajF,Eu');
define('AUTH_SALT',        '=z| >a^ve4RYxo0*D`~[1=pG-VKP)V.b>W{:&Y{52LZ7^cc=)bEl-sl}y!r=t2M:');
define('SECURE_AUTH_SALT', '.?ilIk#Zh5=0@w~IcL0)Z>hJZNZN/bUo-sv,,7:i{e|9NqHj6EhJctYw=uPSY/!I');
define('LOGGED_IN_SALT',   'L&Sai%zg[!Bbg74M(.@sdDcb4A4QjM-9sG5&2H=u1<DH6X<nR|gzbE!Qd;cS#mU3');
define('NONCE_SALT',       '<l`x#  3rwqv)GF:@NiM!:sLsuN3!Qwmr$3@6Wnq+sea&uV|Mr>8TAp2X;zd6YRC');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
